let router = require("../router"),
server = require('../server'),
superagent = require('superagent'),
expect = require('expect.js');
let bl = require('../BL/businessLogic');

describe('server', function () {
    before(function () {
        server.start(router.route, bl);
    });

    describe('requests', function(){


        it('should respond 400 to POST without JSON ',function(done){
            superagent
                .post('http://localhost:8889')
                .end(function(err, res){
                    expect(res.status).to.equal(400);
                    done()
                })
        })

        it('should respond 400 to authenticated POST with wrong JSON ',function(done){
            superagent
                .post('http://localhost:8889')
                .send("sdfgdfg")
                .set({"Content-Type":"application/json"})
                .end(function(err, res){
                    expect(res.status).to.equal(400);
                    done()
                })
        })

        it('should respond 400 to POST with unknown header type ',function(done){
            superagent
                .post('http://localhost:8889')
                .send({			                    
                        "type":"ORDER",
                        "time":"2019-07-01",                    
                        "nomenclature":"",
                        "count":"81"
                })
                .set({"Content-Type":"application/json"})
                .end(function(err, res){
                    expect(res.status).to.equal(400);
                    done()
                })
        })

        it('should respond 200 to  POST with PRODUCTION type ',function(done){
            superagent
            .post('http://localhost:8889')
            .send({			                    
                "type": "PRODUCTION",
                "id": "23943",
                "time": "19.09.18 17:02:22",
                "goods": [
                {
                  "nomenclature": "<6511> МЦ Творог 0%, контейнер 350гр",
                  "count": 200
                } ]
            })
            .set({"Content-Type":"application/json"})
            .end(function(err, res){
                expect(res.status).to.equal(200);
                done()
            })
        })

        it('should respond 200 to  POST with WAREHOUSE type ',function(done){
            superagent
            .post('http://localhost:8889')
            .send({			                    
                "type": "WAREHOUSE",
                "id": "23941",
                "time": "19.09.18 16:42:39",
                "goods": [
                 {
                  "nomenclature": "<1512> МЦ Молоко 2,5% даймонд-керв 1500мл",
                  "count": 1
                 }
                ]
            })
            .set({"Content-Type":"application/json"})
            .end(function(err, res){
                expect(res.status).to.equal(200);
                done()
            })
        })

        it('should respond 200 to GET ',function(done){
            superagent
                .get('http://localhost:8889')
                .end(function(err, res){
                    expect(res.status).to.equal(200);
                    done()
                })
        })

    });
    after(function () {
        server.shutdown();
    });
});