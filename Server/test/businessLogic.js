let bl = require("../BL/businessLogic");
expect = require('expect.js');
const fetch = require('node-fetch');

describe('businessLogic', function () {
    it('выпуск палета', function (done) {
        bl.processPallets({
            "pallets":[
                {
                    "id": "28989",
                    "type": "PRODUCTION",
                    "time": "19.09.30 15:51:50",
                    "palletIndex": "15",
                    "goods": [{
                        "nomenclature": "<5261> КрЦ Сметана 15%, стакан 315гр",
                        "count": 80,
                    } ]
                },
                {
                    "id": "28991",
                    "type": "WAREHOUSE",
                    "time": "19.09.30 15:57:50",
                    "palletIndex": "17",
                    "goods": [{
                        "nomenclature": "<5261> КрЦ Сметана 15%, стакан 315гр",
                        "count": 80,
                    } ]
                }
            ],
            "event": {
                "type": "WAREHOUSEB",
                "palletIndex": "15",
                "goods": [
                 {
                  "nomenclature": "<1461> СтМол Молоко 3,2% даймонд-керв 900гр",
                  "count": 88
                 }
                ]
               }
        }
        );

        let data = bl.getData();

        bl.savesaveProblemPallet(data.pallets[0]);
        bl.savesaveProblemPallet(data.pallets[0]);

        expect(data.events).to.length(1);
        expect(data.pallets).to.length(2);
        data = bl.getData();
        expect(data.events).to.length(1);
        expect(data.pallets).to.length(2);
        done();
    })


});

