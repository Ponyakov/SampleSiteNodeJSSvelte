let requestHandlers = require("./requestHandlers");
let log = require('log4js').getLogger("router");

function route(response, request, bl) {
  log.trace("route start");
  if (request.method == "POST" && request.headers["content-type"] && request.headers["content-type"].indexOf('application/json') > -1) {
    log.trace("Process POST request");
    let data = '';

    request.on('data', function (chunk) {
      data += chunk.toString();
    });

    request.on('end', function () {
      log.info("Data recieved: " + data);

      try {
        let jsonData = JSON.parse(data);

        if (jsonData.pallets) {
          requestHandlers.pallets(response, jsonData, bl);
        } else {
          requestHandlers.error(response, "Wrong data format");
        }
      } catch (err) {
        requestHandlers.error(response, "Wrong data format: " + err.message);
        return;
      }
    });
  } else if (request.method === "GET") {
    log.trace("process GET request");
    requestHandlers.getData(response, bl);
  } else {
    log.info("No request handler found");
    requestHandlers.error(response, "Please send POST request with application/json content type");
  }

  log.trace("route end");
}

exports.route = route;