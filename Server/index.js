let server = require("./server");
let router = require("./router");
let bl = require("./BL/businessLogic")

let log4js = require('log4js');
log4js.configure('./config/log4js.json');

server.start(router.route, bl);

