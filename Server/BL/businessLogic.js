const log = require('log4js').getLogger("businessLogic");
const sn = require('./sendNotifications');
const fsPromises = require('fs').promises;
const path = require('path');

const PROBLEM_WAIT_TIME = 30 // минут

let pallets = [];
let events = [];

function processPallets(jsonData) {
	log.trace("processPallets start");

	addNewEvent(jsonData);

	// обновляем палеты
	updatePallets(jsonData);

	// удаляем палеты 
	removePallets(jsonData);

	// помечаем просроченые 
	markExpared();

	// удаляем неактуальные события и обновляем палеты
	removeEventsAndUpdatePallets();

	log.trace("processPallets end");
}

function getData() {
	log.trace("getData start");
	let data = {
		events: events,
		pallets: pallets,
	}

	log.trace("getData end");
	return data;
}

function sendNotifications() {
	sn.sendNotifications(pallets);
}

function addNewEvent(jsonData) {
	log.trace("addNewEvent start");

	if (jsonData.event && jsonData.event.palletIndex) {
		let newEvent = {
			type: jsonData.event.type,
			palletIndex: jsonData.event.palletIndex,
			nomenclature: jsonData.event.goods.length > 0 ? jsonData.event.goods[0].nomenclature : "",
			count: jsonData.event.goods.length > 0 ? Number(jsonData.event.goods[0].count) : 0,
		};

		events.push(newEvent);

		log.trace("добавляется событие:");
		log.trace(newEvent);
	}

	log.trace("addNewEvent end");
}

function updatePallets(jsonData) {
	log.trace("updatePallets start");

	for (let i = 0; i < jsonData.pallets.length; i++) {
		let found = false;

		for (let j = 0; j < pallets.length; j++) {

			if (jsonData.pallets[i].id === pallets[j].id) {
				pallets[j].palletIndex = jsonData.pallets[i].palletIndex;
				pallets[j].date = jsonData.pallets[i].time;
				pallets[j].nomenclature = jsonData.pallets[i].goods[0].nomenclature;

				if (jsonData.pallets[i].type === "PRODUCTION") {
					pallets[j].productionCount = Number(jsonData.pallets[i].goods[0].count);
				} else {
					pallets[j].warehouseCount = Number(jsonData.pallets[i].goods[0].count);
				}

				found = true;
				break;
			}
		}

		if (!found) {
			let newPallet = {
				id: jsonData.pallets[i].id,
				palletIndex: jsonData.pallets[i].palletIndex,
				date: jsonData.pallets[i].time,
				timestamp: Date.now(),
				nomenclature: jsonData.pallets[i].goods[0].nomenclature,
				productionCount: 0,
				warehouseCount: 0,
				productionDefect: false,
				warehouseDefect: false,
				expared: false,
				sent: false,
				problemTime: 0,
			};

			if (jsonData.pallets[i].type === "PRODUCTION") {
				newPallet.productionCount = Number(jsonData.pallets[i].goods[0].count);
			} else {
				newPallet.warehouseCount = Number(jsonData.pallets[i].goods[0].count);
			}

			log.trace("добавляется палета:");
			log.trace(newPallet);

			pallets.push(newPallet);
		}
	}

	log.trace("updatePallets end");
}

function removePallets(jsonData) {
	log.trace("removePallets start");

	for (let i = 0; i < pallets.length; i++) {
		let found = false;
		for (let j = 0; j < jsonData.pallets.length; j++) {
			if (pallets[i].id === jsonData.pallets[j].id) {
				found = true;
				break;
			}
		}

		if (!found) {
			log.trace("Удаляется палета:");
			log.trace(pallets[i]);

			saveProblemPallet(pallets[i]);

			pallets.splice(i, 1);
			i--;
		}
	}

	log.trace("removePallets end");
}

function markExpared() {
	log.trace("markExpared start");

	let currentTime = new Date();

	for (let i = 0; i < pallets.length; i++) {
		if (pallets[i].expared) {
			continue;
		}


		if ((currentTime - pallets[i].timestamp) > PROBLEM_WAIT_TIME * 60 * 1000) {
			pallets[i].expared = true;
			pallets[i].problemTime = new Date();
		}
	}
	log.trace("markExpared end");
}

function removeEventsAndUpdatePallets() {
	log.trace("removeEventsAndUpdatePallets start");

	for (let j = 0; j < events.length; j++) {
		let found = false;
		for (let i = 0; i < pallets.length; i++) {
			if (events[j].palletIndex === pallets[i].palletIndex) {
				found = true;

				if (events[j].type === "PRODUCTION") {
					pallets[i].warehouseCount = events[j].count;
				} else if (events[j].type === "WAREHOUSE") {
					pallets[i].productionCount = events[j].count;
				}
				else if (events[j].type === "PRODUCTIONB") {
					pallets[i].productionDefect = true;
					pallets[i].problemTime = new Date();
				}
				else if (events[j].type === "WAREHOUSEB") {
					pallets[i].warehouseDefect = true;
					pallets[i].problemTime = new Date();
				}

			}
		}

		if (found === false) {
			log.trace("Удаляется событие:");
			log.trace(events[j]);

			events.splice(j, 1);
			j--;
		}
	}

	log.trace("removeEventsAndUpdatePallets end");
}

function saveProblemPallet(pallet) {
	log.trace("saveProblemPallet start");

	if (pallet.expared || pallet.productionDefect || pallet.warehouseDefect) {
		let currentTime = new Date();
		let row = pallet.palletIndex + "\t" + pallet.nomenclature + "\t" + (pallet.productionCount > 0 ? pallet.productionCount : pallet.warehouseCount) +
			"\t" + pallet.date + "\t" + formateTime(pallet.problemTime) + "\t" + formateTime(currentTime) + "\t" +
			(pallet.expared ? "Продукция находилась в буфере дольше 30 минут. " : "") + (pallet.productionDefect ? " БРАК. " : "") +
			(pallet.warehouseDefect ? " БРАК НА СКЛАДЕ. " : "") + "\n\r"

		let fileName = currentTime.getFullYear().toString() + "_" + ("0" + (currentTime.getMonth() + 1)).slice(-2) + "_" + ("0" + (currentTime.getDate())).slice(-2) + ".csv"
		let fullPath = path.join('\\192.168.0.7\\admin\\Volume_2\\Документы\\Производство\\Отчеты', fileName);

		log.info(currentTime);
		log.info(fileName);

		fsPromises.appendFile(fullPath, row, "utf8")
			.then(() => {
				log.Info("сделана запись в лог с проблемными палетами");
			})
			.catch(error => {
				log.error(error);
			})
	}

	log.trace("saveProblemPallet end");
}

function formateTime(date) {
	return ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2);
}

exports.processPallets = processPallets;
exports.getData = getData;
exports.sendNotifications = sendNotifications;
exports.savesaveProblemPallet = saveProblemPallet;
