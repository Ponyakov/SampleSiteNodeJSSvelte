let log = require('log4js').getLogger("sendNotifications");
let nodemailer = require('nodemailer');

const PROBLEM_RESOLVE_TIME = 30 // минут


function sendNotifications(pallets) {
    log.trace("sendNotifications start");
    try {

        let currentTime = new Date();

        for (let i = 0; i < pallets.length; i++) {
            if (pallets[i].sent) {
                continue;
            }

            if (pallets[i].productionDefect || pallets[i].warehouseDefect) {
                if ((currentTime - pallets[i].problemTime) > PROBLEM_RESOLVE_TIME * 60 * 1000) {
                    log.info("Проблемный палет:");
                    log.info(pallets[i]);
                    sendNotResolved(pallets[i]);
                    pallets[i].sent = true;
                }

            } else if (pallets[i].expared) {
                log.info("Проблемный палет:");
                log.info(pallets[i]);
                
                sendExpared(pallets[i]);
                pallets[i].sent = true;
            }
        }
    }
    catch(error)
    {
        log.error(error);
    }
    log.trace("sendNotifications end");
}

function sendExpared(pallet) {
    log.trace("sendExpared start");

    var body = "Паллет " + pallet.palletIndex + " находится в буферной зоне больше 30 минут." +
        "\n\rПродукция: " + pallet.nomenclature +
        "\n\rКоличество:  " + (pallet.productionCount == 0 ? pallet.warehouseCount : pallet.productionCount);

    send(body);

    log.trace("sendExpared end");
}

function sendNotResolved(pallet) {
    log.trace("sendNotResolved start");

    var body = "Паллет " + pallet.palletIndex + " находится в проблемном статусе больше 30 минут." +
        "\n\rПродукция: " + pallet.nomenclature +
        "\n\rКоличество производство:  " + pallet.productionCount +
        "\n\rКоличество склад:  " + pallet.warehouseCount +
        (pallet.productionDefect ? "\n\rБРАК" : "") +
        (pallet.warehouseDefect ? "\n\rБРАК НА СКЛАДЕ" : "");

    send(body);

    log.trace("sendNotResolved end");
}

function send(body) {
    log.trace("send start");

    let message = {
        from: 'sender@tmktv.ru',
        to: 'd.nikitenko@tmktv.ru,a.savelev@tmktv.ru',
        subject: 'Проблемный палет',
        text: body,
    };

    let transport = nodemailer.createTransport({
        pool: true,
        host: "smtp.yandex.ru",
        port: 465,
        secure: true, // use TLS
        auth: {
            user: "sender@tmktv.ru",
            pass: "giperion12345"
        }
    });

    log.info("отправляем письмо");
    log.info(message);

    transport.sendMail(message)
        .then(info => {
            log.info("отправлено оповещение: ");
            log.info(message);
            log.debug("ответ почтового сервера: ");
            log.debug(info);
        })
        .catch(err => {
            log.error("ошибка отправки электронного письма:");
            log.error(err);
        });

    log.trace("send end");
}

exports.sendNotifications = sendNotifications;
exports.send = send;