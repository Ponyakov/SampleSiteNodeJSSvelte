let http = require("http");
let log = require('log4js').getLogger("server");
require('http-shutdown').extend();


let server;
let interval;

function start(route, bl) {
  function onRequest(request, response) {
    log.debug("Request  received.");
    response.writeHead(200);
    log.debug("method: ");
    log.debug(request.method);
    log.debug("headers: ");
    log.debug(request.headers);
    log.debug("url: ");
    log.debug(request.url);

    route(response, request, bl);

    interval = setInterval(() => {
      bl.sendNotifications();
    }, 60 * 1000);
  }


  server = http.createServer(onRequest).listen(8889).withShutdown();
  server.on('error', function (err) {
    log.error(err);
  });

  process.on('uncaughtException', function (err) {
    log.error('uncaughtException: ', err.message);
    log.error(err.stack);
    process.exit(1);
  });



  log.info("Server listening on port 8889.");
}

function shutdown() {
  server.shutdown(function () {
    log.info("Server has been shutdown.");
    conole.log("Server has been shutdown.")
  })

  clearInterval(interval);
}

exports.start = start;
exports.shutdown = shutdown;