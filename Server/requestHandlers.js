let log = require('log4js').getLogger("requestHandlers");


function pallets(response, jsonData, bl) {
	log.trace("pallets start");

	log.debug("Request handler 'production' was called.");

	bl.processPallets(jsonData);

	response.writeHead(200, { "Content-Type": "text/html" });
	response.write('accepted');
	response.end();

	log.trace("pallets end");
}

function getData(response, bl) {
	log.trace("getData start");
	log.debug("Request handler 'getData' was called.")

	let data = bl.getData();

	response.writeHead(200, { "Content-Type": "application/json; charset=utf-8",
		"Access-Control-Allow-Origin": "*" });
	response.write(JSON.stringify(data));
	response.end();

	log.trace("getData end");
}

function error(response, message) {
	log.trace("error start");

	log.debug("Request handler 'error' was called.");
	log.error(message);

	response.writeHead(400, { "Content-Type": "text/html" });
	response.write(message);
	response.end();

	log.trace("error end");
}

function serverError(response, message) {
	log.trace("serverError start");

	log.debug("Request handler 'serverError' was called.");
	log.error(message);

	response.writeHead(500, { "Content-Type": "text/html" });
	response.write(message);
	response.end();

	log.trace("serverError end");
}

exports.pallets = pallets;
exports.getData = getData;
exports.error = error;
exports.serverError = serverError;