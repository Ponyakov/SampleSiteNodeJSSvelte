
import { readable } from 'svelte/store';

function getData() {
  return fetch("http://192.168.0.10:8889")
    .then(res => {
      if (!res.ok) {
        throw new Error("Failed!");
      }

      return res.json();
    })
    .then(data => {
      return data;
    })
    .catch(err => {
      console.log(err);
    });
}

const dataStore = readable({
  events: [],
  pallets: []
}
, function start(set) {

  const interval = setInterval(() => {
    getData().then(data => {
      set(data);
    })
  }, 1000);

  return function stop() {

    clearInterval(interval);

  };
});

export default dataStore;
