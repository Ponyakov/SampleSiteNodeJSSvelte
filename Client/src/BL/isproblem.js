export default function isProblem(pallet) {
  if (pallet.productionCount > 0 && pallet.warehouseCount > 0) {
    return true;
  }

  if (pallet.productionDefect === true || pallet.warehouseDefect === true || pallet.expared === true) {
    return true;
  }

  return false;
}