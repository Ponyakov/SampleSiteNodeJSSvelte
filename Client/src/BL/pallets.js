function getAddedPallets(oldPallets, newPallets) {
    let addedPallets = [];

    for (let i = 0; i < newPallets.length; i++) {
        
        let found = false;
        for (let j = 0; j < oldPallets.length; j++) {
            if (newPallets[i].id == oldPallets[j].id) {
                found = true;
                break;
            }
        }
        if (!found) {
            addedPallets.push(newPallets[i]);
        }
    }

    return addedPallets;
}

function getDeletedPallets(oldPallets, newPallets) {
    let deletedPallets = [];

    for (let i = 0; i < oldPallets.length; i++) {
        
        let found = false;
        for (let j = 0; j < newPallets.length; j++) {
            if (oldPallets[i].id == newPallets[j].id) {
                found = true;
                break;
            }
        }
        if (!found) {
            deletedPallets.push(oldPallets[i]);
        }
    }

    return deletedPallets;
}

export { getAddedPallets, getDeletedPallets }